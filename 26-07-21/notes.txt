BS Typography 
    text-center text-right text-justify text-left 
    font-italic font-weight-bold text-danger

BS Colors
    text-  bg- 
    success 
    info 
    warning
    danger 
    light
    dark 
    primary
    secondary
    white 
    muted (only for text )

BS Margin/Padding (1rem = 16px)
    m-1 p-5
    mt, mb, mr, ml, pt, pr, pb, pl 
    my, mx, py, px

xs (for phones - screens less than 768px wide)
sm (for tablets - screens equal to or greater than 768px wide)
md (for small laptops - screens equal to or greater than 992px wide)
lg (for laptops and desktops - screens equal to or greater than 1200px wide)


12 = 100% 
6 = 50%
3 = 25%
4 = 33%